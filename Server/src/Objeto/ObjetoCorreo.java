/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objeto;

import java.io.Serializable;




public class ObjetoCorreo implements Serializable{

  private String user;
  private String pass;
  private String mensaje;
  private String attch;
  private String to;

    public ObjetoCorreo( String userr,String passs,String mensaje, String attch, String to) {
        this.user = userr;
        this.pass = passs;
        this.mensaje = mensaje;
        this.attch = attch;
        this.to = to;
    }
  
  

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the pass
     */
    public String getPass() {
        return pass;
    }

    /**
     * @param pass the pass to set
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the attch
     */
    public String getAttch() {
        return attch;
    }

    /**
     * @param attch the attch to set
     */
    public void setAttch(String attch) {
        this.attch = attch;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }
  
  
  

    
}
