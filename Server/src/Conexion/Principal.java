package Conexion;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import Objeto.ObjetoCorreo;
import CorreoElectronico.SendCorreo;


public class Principal {

    final int PUERTO = 1234;
    final String HOST = "localhost";
    protected ServerSocket serverSocket;
    protected ObjectInputStream dataObject;
    protected Socket socket;
    
    
    
//SERVIDOR
    public void initServer() throws ClassNotFoundException {

    try{
    this.serverSocket = new ServerSocket(this.PUERTO);
    this.socket = new Socket();
    
    while(true){
        System.out.println("Esperando");
        this.socket=this.serverSocket.accept();
        System.out.println("Cliente en linea");
        this.dataObject=new ObjectInputStream(this.socket.getInputStream());
        System.out.println("Objeto recibido");
        ObjetoCorreo o = (ObjetoCorreo) this.dataObject.readObject();
        
       //Envio de correo electronico
       SendCorreo send = new SendCorreo(o);
       send.send();
        
      System.out.println("Fin de la conexion");
    }  
        
    }catch(IOException ex){
        System.out.println("Error : "+ex);   
    }

    }

}
