package CorreoElectronico;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Ejemplo de envio de un correo de texto con una imagen adjunta con javamail
 *
 * @author Chuidiang
 *
 */
public class SendCorreo {

    private Objeto.ObjetoCorreo datos;

    public SendCorreo(Objeto.ObjetoCorreo data) {
        this.datos = data;
    }

    public void send() {
        try {
            // se obtiene el objeto Session. La configuración es para
            // una cuenta de gmail.
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", getDatos().getUser());
            props.setProperty("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(props, null);
            // session.setDebug(true);

            // Se compone la parte del texto
            BodyPart texto = new MimeBodyPart();
            texto.setText(getDatos().getMensaje());

            // Se compone el adjunto con la imagen
            BodyPart adjunto = new MimeBodyPart();
            adjunto.setDataHandler(new DataHandler(new FileDataSource(getDatos().getAttch())));
            adjunto.setFileName("Cierre Caja Automatico");

            // Una MultiParte para agrupar texto e imagen.
            MimeMultipart multiParte = new MimeMultipart();
            multiParte.addBodyPart(texto);
            multiParte.addBodyPart(adjunto);

            // Se compone el correo, dando to, from, subject y el
            // contenido.
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(getDatos().getUser()));
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress(getDatos().getTo()));
            message.setSubject("Cierre Caja");
            message.setContent(multiParte);

            // Se envia el correo.
            Transport t = session.getTransport("smtp");
            t.connect(getDatos().getUser(), getDatos().getPass());
            t.sendMessage(message, message.getAllRecipients());
            t.close();
            System.out.println("Correo Enviado Correctamente");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Objeto.ObjetoCorreo getDatos() {
        return datos;
    }

    public void setDatos(Objeto.ObjetoCorreo datos) {
        this.datos = datos;
    }

}
